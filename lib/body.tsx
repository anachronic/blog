import styled from '@emotion/styled'

export const Body = styled.body`
  margin: 0;
  font-family: Avenir, Arial, Helvetica, sans-serif;
  line-height: 1.5;
`
