import styled from '@emotion/styled'

export const Footer = styled.footer`
  text-align: center;
  margin-top: 2em;
  margin-bottom: 1em;
`
