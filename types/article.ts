export interface Article {
  date: string
  slug: string
  title: string
}
